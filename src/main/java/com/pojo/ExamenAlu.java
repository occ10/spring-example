package com.pojo;

import com.entity.Alumno;
import com.entity.CourseRegistration;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExamenAlu {

	private String nombre;
	private Long cursoId;
	private Double nota;

	public ExamenAlu() {

	}

	public ExamenAlu(CourseRegistration curso) {
		this.nombre = curso.getAlumnos().getNombre();
		this.cursoId = curso.getId();
		this.nota = curso.getGrade();
	}

}
