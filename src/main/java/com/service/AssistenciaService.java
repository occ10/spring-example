package com.service;

import java.util.List;

import com.entity.FaltaAssistencia;

public interface AssistenciaService {

	public void sendEmail();
	public List<FaltaAssistencia> getFaltas();
}
