package com.service;

import java.util.List;

import com.entity.Alumno;
import com.entity.AsignaturaAlumno;
import com.pojo.AluAssist;


public interface AlumnoService {

	public Alumno saveOrUpdate(Alumno alumno);
	
	public List<Alumno> saveOrUpdateAll(List<Alumno> alumno);
	
	public Alumno findById(Long id);
	
	public List<Alumno> getAllAlumnos();

	public void deleteById(Long id);
	
	void asignAbscence(AluAssist aluAsist);
	
	void aluAsig(AsignaturaAlumno aluAsig);
}
