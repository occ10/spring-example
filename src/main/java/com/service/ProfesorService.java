package com.service;

import java.util.List;

import com.entity.Profesor;


public interface ProfesorService {

	public Profesor saveOrUpdate(Profesor profesor);
	
	public List<Profesor> saveOrUpdateAll(List<Profesor> profesor);
	
	public Profesor findById(Long id);
	
	public List<Profesor> getAllProfesores();

	public void deleteById(Long id);
}
