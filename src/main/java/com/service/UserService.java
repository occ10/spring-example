package com.service;

import java.util.List;

import com.entity.User;

public interface UserService {

	User findById(Long id);

	User saveOrUpdate(User user);
	
	void deleteById(Long id);
	
	void deleteUser(Long id);
	
	List<User> findAll();
	
	void addLastNameColumn();

	void deleteUserById(Long id);
	
}
