package com.service;

import java.util.List;

import com.dto.CourseCreationDto;
import com.entity.CourseRegistration;

public interface CourseRegistrationService {

	public List<CourseRegistration> findByAsig(Long idAsig);

	void setgradeOfStudent(CourseCreationDto notasAlumnos);

}
