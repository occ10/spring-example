package com.service;

import java.util.List;

import com.entity.Asignatura;

public interface AsignaturaService {

	public Asignatura saveOrUpdate(Asignatura asignatura);
	
	public List<Asignatura> saveOrUpdateAll(List<Asignatura> asignaturas);
	
	public Asignatura findById(Long id);
	
	public List<Asignatura> getAllColegios();

	public void deleteById(Long id);

}
