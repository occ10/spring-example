package com.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entity.Asignatura;
import com.repository.AsignaturaRepository;
import com.service.AsignaturaService;

@Service
public class AsignaturaServiceImpl implements AsignaturaService {

	@Autowired
	private AsignaturaRepository asignaturaRepository;

	@Override
	public Asignatura saveOrUpdate(Asignatura asignatura) {
		return this.asignaturaRepository.save(asignatura);

	}

	@Override
	public List<Asignatura> saveOrUpdateAll(List<Asignatura> asignaturas) {
		return this.asignaturaRepository.saveAll(asignaturas);

	}

	@Override
	public Asignatura findById(Long id) {
		Optional<Asignatura> asignatura = this.asignaturaRepository.findById(id);
		if (asignatura.isPresent())
			return asignatura.get();
		else
			return null;
	}

	@Override
	public List<Asignatura> getAllColegios() {
		return this.asignaturaRepository.findAll();
	}

	@Override
	public void deleteById(Long id) {
		this.asignaturaRepository.deleteById(id);

	}

}