package com.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.components.GestorCorreo;
import com.entity.FaltaAssistencia;
import com.repository.AssistenciaRepository;
import com.service.AssistenciaService;

@Service
public class AssistenciaServiceImpl implements AssistenciaService{

	@Autowired
	private GestorCorreo gestorCorreo;
	
	@Autowired
	private AssistenciaRepository assistenciaRepository;
	
	@Override
	public void sendEmail() {
		List<FaltaAssistencia> faltas = this.assistenciaRepository.findAll();
		for (FaltaAssistencia f: faltas) {
			if(!f.isEnviado()) {
				
			}
		}
		gestorCorreo.sendSimpleMessage("josedavidvelascoandreu@gmail.com", "occ10", "eres un crack que a veces tienes mala leche");
		
	}

	@Override
	public List<FaltaAssistencia> getFaltas() {
		return this.assistenciaRepository.findAll();
	}

}
