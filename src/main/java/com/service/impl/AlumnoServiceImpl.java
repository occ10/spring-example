package com.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entity.Alumno;
import com.entity.Asignatura;
import com.entity.AsignaturaAlumno;
import com.entity.CourseRegistration;
import com.entity.FaltaAssistencia;
import com.pojo.AluAssist;
import com.repository.AlumnoRepository;
import com.repository.AsignaturaRepository;
import com.repository.AssistenciaRepository;
import com.repository.CourseRegistrationRipository;
import com.service.AlumnoService;

@Service
public class AlumnoServiceImpl implements AlumnoService {

	@Autowired
	private AlumnoRepository alumnoRepository;

	@Autowired
	AssistenciaRepository assistenciaRepository;

	@Autowired
	AsignaturaRepository asignaturaRepository;

	@Autowired
	private CourseRegistrationRipository courseRegistrationRipository;

	@Override
	public Alumno saveOrUpdate(Alumno alumno) {
		return this.alumnoRepository.save(alumno);

	}

	@Override
	public List<Alumno> saveOrUpdateAll(List<Alumno> alumnos) {
		return this.alumnoRepository.saveAll(alumnos);

	}

	@Override
	public Alumno findById(Long id) {
		Optional<Alumno> alumno = this.alumnoRepository.findById(id);
		if (alumno.isPresent())
			return alumno.get();
		else
			return null;
	}

	@Override
	public List<Alumno> getAllAlumnos() {
		return this.alumnoRepository.findAll();
	}

	@Override
	public void deleteById(Long id) {
		this.alumnoRepository.deleteById(id);

	}

	@Override
	public void asignAbscence(AluAssist aluAsist) {
		Alumno alumno = this.alumnoRepository.findById(aluAsist.getAlumnoId())
				.orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + aluAsist.getAlumnoId()));
		FaltaAssistencia assist = new FaltaAssistencia();
		assist.setDateTime(aluAsist.getDateTime());
		assist.setAlumno(alumno);
		this.assistenciaRepository.save(assist);

	}

	@Override
	public void aluAsig(AsignaturaAlumno aluAsig) {

		Alumno alumno = this.alumnoRepository.findById(aluAsig.getAlumnoId())
				.orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + aluAsig.getAlumnoId()));
		Asignatura asignatura = this.asignaturaRepository.findById(aluAsig.getAsignaturaId())
				.orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + aluAsig.getAsignaturaId()));

		CourseRegistration cursoAlumno = new CourseRegistration();
		cursoAlumno.setAlumnos(alumno);
		cursoAlumno.setAsignaturas(asignatura);
		this.courseRegistrationRipository.save(cursoAlumno);

	}
}
