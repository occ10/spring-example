package com.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dto.CourseCreationDto;
import com.entity.CourseRegistration;
import com.pojo.ExamenAlu;
import com.repository.CourseRegistrationRipository;
import com.service.CourseRegistrationService;

@Service
public class CourseRegistrationServiceImpl implements CourseRegistrationService{

	@Autowired
	CourseRegistrationRipository courseRegistrationRipository;
	
	@Override
	public List<CourseRegistration> findByAsig(Long idAsig) {
		return this.courseRegistrationRipository.findByAsig(idAsig);
	}

	@Override
	public void setgradeOfStudent(CourseCreationDto notasAlumnos) {
		
		for(ExamenAlu examAlu : notasAlumnos.examenAlu) {
			this.courseRegistrationRipository.setgradeOfStudent(examAlu.getNota(), examAlu.getCursoId());
		}
		
	}

}
