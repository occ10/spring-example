package com.service.impl;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.entity.User;
import com.repository.UserRepository;
import com.service.UserService;

import exception.NotFoundException;

@Service
///@Transactional(readOnly = true)
public class UserServiceImpl implements UserService {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);
	
	@Autowired
	UserRepository userRepository;

	@Override
	public User findById(final Long id) {
		LOGGER.debug("Getting User with id {}", id);
		User user = userRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
		if (user == null) {
			LOGGER.error("User with id {} not found", id);
			throw new NotFoundException("User con id " + id + " no encontrada");
		}
		return user;
	}

	@Override
	@Transactional(readOnly = false)
	public User saveOrUpdate(User user) {
		if (user.getId() != null) {
			LOGGER.debug("User {} with id {}", user, user.getId());
			this.findById(user.getId());
		} else {
			LOGGER.debug("Saving new User {}", user);
		}
		return this.userRepository.save(user);
	}
	
	@Override
	public void deleteById(final Long id) {
		LOGGER.debug("Getting User with id {}", id);
		Optional<User> user = userRepository.findById(id);
		if (!user.isPresent()) {
			LOGGER.error("User with id {} not found", id);
			throw new NotFoundException("User con id " + id + " no encontrada");
		}
		
		userRepository.deleteById(id);
	
	}

	@Override
	public void deleteUser(Long id) {
		LOGGER.debug("Getting User with id {}", id);
		User user = userRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
		if (user == null) {
			LOGGER.error("User with id {} not found", id);
			throw new NotFoundException("User con id " + id + " no encontrada");
		}
		
		LOGGER.debug("Delete user {}", id);
		userRepository.delete(user);
	
	}
	
	@Override
	public List<User> findAll() {
		return (List<User>) userRepository.findAll();
	}

	@Override
	@Transactional
	public void addLastNameColumn() {
		
		userRepository.addLastNameColumn();
		
	}
	
	@Override
	@Transactional
	public void deleteUserById(Long id) {
		LOGGER.debug("Getting User with id {}", id);
		User user = userRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
		if (user == null) {
			LOGGER.error("User with id {} not found", id);
			throw new NotFoundException("User con id " + id + " no encontrada");
		}
		
		LOGGER.debug("Delete user {}", id);
		userRepository.deleteUserById(id);
	
	}

}
