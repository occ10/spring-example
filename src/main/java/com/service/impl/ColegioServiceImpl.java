package com.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entity.Colegio;
import com.repository.ColegioRepository;
import com.service.ColegioService;

@Service
public class ColegioServiceImpl implements ColegioService {

	@Autowired
	private ColegioRepository colegioRepository;

	@Override
	public Colegio saveOrUpdate(Colegio colegio) {
		return this.colegioRepository.save(colegio);

	}

	@Override
	public List<Colegio> saveOrUpdateAll(List<Colegio> colegios) {
		return this.colegioRepository.saveAll(colegios);

	}

	@Override
	public Colegio findById(Long id) {
		Optional<Colegio> colegio = this.colegioRepository.findById(id);
		if (colegio.isPresent())
			return colegio.get();
		else
			return null;
	}

	@Override
	public List<Colegio> getAlllColegios() {
		return this.colegioRepository.findAll();
	}

	@Override
	public void deleteById(Long id) {
		this.colegioRepository.deleteById(id);

	}
}
