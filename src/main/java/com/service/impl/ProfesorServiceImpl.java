package com.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entity.Profesor;
import com.repository.ProfesorRepository;
import com.service.ProfesorService;

@Service
public class ProfesorServiceImpl implements ProfesorService {

	@Autowired
	private ProfesorRepository profesorRepository;

	@Override
	public Profesor saveOrUpdate(Profesor profesor) {
		return this.profesorRepository.save(profesor);

	}

	@Override
	public List<Profesor> saveOrUpdateAll(List<Profesor> profesores) {
		return this.profesorRepository.saveAll(profesores);

	}

	@Override
	public Profesor findById(Long id) {
		Optional<Profesor> profesor = this.profesorRepository.findById(id);
		if (profesor.isPresent())
			return profesor.get();
		else
			return null;
	}

	@Override
	public List<Profesor> getAllProfesores() {
		return this.profesorRepository.findAll();
	}

	@Override
	public void deleteById(Long id) {
		this.profesorRepository.deleteById(id);

	}
}
