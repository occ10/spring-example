package com.service;

import java.util.List;

import com.entity.Colegio;

public interface ColegioService {

	public Colegio saveOrUpdate(Colegio colegio);
	
	public List<Colegio> saveOrUpdateAll(List<Colegio> colegios);
	
	public Colegio findById(Long id);
	
	public List<Colegio> getAlllColegios();

	public void deleteById(Long id);
}
