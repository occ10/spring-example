package com.dto;

import java.util.ArrayList;
import java.util.List;

import com.pojo.ExamenAlu;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CourseCreationDto {

    public List<ExamenAlu> examenAlu;
    
    public CourseCreationDto() {
    	examenAlu = new ArrayList<>();
    }
 
    public void addExamenalu(ExamenAlu examenAlu) {
        this.examenAlu.add(examenAlu);
    }
}
