package com.alumnoResponse;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.entity.Alumno;
import com.enums.Sexo;

public class AlumnoResponse {
	
	private Long id;
	private String dni;
	private String nombre;
	private String apellidos;
	private int edad;
	private String sexo;
	private String emailContacto;
	
	public AlumnoResponse() {
		
	}

	public AlumnoResponse(Alumno alumno) {
		super();
		this.id = alumno.getId();
		this.dni = alumno.getDni();
		this.nombre = alumno.getNombre();
		this.apellidos = alumno.getApellidos();
		this.edad = alumno.getEdad();
		this.sexo = alumno.getSexo().toString();
		this.emailContacto = alumno.getEmailContacto();
	}

	
}
