package com.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class CourseRegistration {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    Long id;
 
    @ManyToOne
    @JoinColumn(name = "student_id")
    Alumno alumnos;
 
    @ManyToOne
    @JoinColumn(name = "course_id")
    Asignatura asignaturas;
 
    Double grade;
}
