package com.entity;

import lombok.Data;

@Data
public class DeleteResponse {

	private String message;
	
	public DeleteResponse(String _message) {
		this.message = _message;
	}

}
