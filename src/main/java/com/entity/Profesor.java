package com.entity;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "profesor")
public class Profesor extends Persona implements Serializable {

	private static final long serialVersionUID = 1L;

	@ManyToMany
	@JoinTable(name = "pro_asig", joinColumns = @JoinColumn(name = "profesor_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "asignatura_id", referencedColumnName = "id"))
	private Collection<Asignatura> asignaturaspro;

}
