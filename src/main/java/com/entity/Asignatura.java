package com.entity;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString
@Entity
@Table(name = "asignatura")
public class Asignatura implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "nombre")
	private String nombre;

	@Column(name = "siglas")
	private String siglas;

	@Column(name = "horaslectivas")
	private int horaslectivas;

	@Column(name = "codigo")
	private String codigo;

	@OneToMany(mappedBy = "asignaturas")
	Collection<CourseRegistration> registros;

	@ManyToMany(mappedBy = "asignaturaspro")
	private Collection<Profesor> profesores;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;

		return siglas.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Asignatura other = (Asignatura) obj;
		if (siglas == null) {
			if (other.siglas != null)
				return false;
		} else if (!siglas.equals(other.siglas))
			return false;
		return true;
	}

}
