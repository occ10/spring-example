package com.entity;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "alumno")
public class Alumno extends Persona implements Serializable {

	private static final long serialVersionUID = 1L;

    @OneToMany(mappedBy = "alumnos")
    Collection<CourseRegistration> registros;
    
	@OneToMany(mappedBy="alumno")
    private Collection<FaltaAssistencia> faltas;
}
