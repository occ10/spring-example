package com.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
public class AsignaturaAlumno {
	
	private Long asignaturaId;
	private Long alumnoId;
}
