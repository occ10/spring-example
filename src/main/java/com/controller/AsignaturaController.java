package com.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.entity.Asignatura;
import com.service.impl.AsignaturaServiceImpl;

import exception.NotFoundException;

@RestController
@RequestMapping("/asignatura")
public class AsignaturaController {

	@Autowired
	private AsignaturaServiceImpl asignaturaService;

	private static final Logger LOGGER = LoggerFactory.getLogger(AsignaturaController.class);

	@GetMapping(value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public Asignatura findById(@PathVariable(value = "id") Long id) {
		
		LOGGER.info("AsignaturaController, Method 'findById'({})'", id);
		return asignaturaService.findById(id);
	}

	@PostMapping(produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseStatus(HttpStatus.CREATED)
	public Asignatura save(@RequestBody final Asignatura asignatura) {
		
		LOGGER.info("AsignaturaController, Method 'save'({})'", asignatura.getId());
		return asignaturaService.saveOrUpdate(asignatura);
	}

	@PostMapping(value = "/save", produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseStatus(HttpStatus.CREATED)
	public List<Asignatura> saveAll(@RequestBody final List<Asignatura> asignaturas) {
		
		LOGGER.info("AsignaturaController, Method 'saveAll'({})'", "");
		return asignaturaService.saveOrUpdateAll(asignaturas);
	}

	@PutMapping(value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public Asignatura update(@RequestBody final Asignatura asignatura, @PathVariable final Long id) {
		
		LOGGER.info("AsignaturaController, Method 'update'({})'", "");
		return this.asignaturaService.saveOrUpdate(asignatura);
	}

	@DeleteMapping(value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public void delete(@PathVariable final Long id) {
		
		LOGGER.info("AsignaturaController, Method 'delete'({})'", "");

		try {
			this.asignaturaService.deleteById(id);
		} catch (IllegalArgumentException e) {
			throw new NotFoundException("asignatura con id " + id + " no se ha podido borrar");
		}
	}
}
