package com.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.entity.AsignaturaAlumno;
import com.service.AlumnoService;
import com.service.AsignaturaService;

@Controller
public class AluAsigController {

	private static final Logger LOGGER = LoggerFactory.getLogger(AluAsigController.class);

	@Autowired
	AsignaturaService asignaturaService;

	@Autowired
	AlumnoService alumnoService;

	@RequestMapping(value = "/consulta", method = RequestMethod.GET)
	public String consulta(Model model) {
		
		LOGGER.info("AluAsigController, Method 'adduser'({})'", "");
		model.addAttribute("asignaturas", this.asignaturaService.getAllColegios());
		model.addAttribute("alumnos", this.alumnoService.getAllAlumnos());
		model.addAttribute("aluAsig", new AsignaturaAlumno());

		return "asignatura-alumno";
	}

	@RequestMapping(value = "/salvar", method = RequestMethod.POST)
	public String salvar(AsignaturaAlumno aluAsig) {
		
		LOGGER.info("AluAsigController, Method 'salvar'({})'", aluAsig.getAlumnoId() + " " + aluAsig.getAsignaturaId());
		this.alumnoService.aluAsig(aluAsig);

		return "index-asignaturas";
	}

}
