package com.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.service.AssistenciaService;

@Controller
@RequestMapping("/assistencia")
public class AssistenciaController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AssistenciaController.class);
	
	@Autowired
	private AssistenciaService assistenciaService;
	
	@GetMapping
	public String sendMail() {
		
		LOGGER.info("AssistenciaController, Method 'sendMail'({})'", "");

		this.assistenciaService.sendEmail();
		
		return "index";
	}	

}
