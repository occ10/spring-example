package com.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.entity.Asignatura;
import com.service.AsignaturaService;

@Controller
public class AsigController {

	private static final Logger LOGGER = LoggerFactory.getLogger(AsigController.class);

	@Autowired
	private AsignaturaService asignaturaService;

	@GetMapping("/crear")
	public String showSignUpForm(Asignatura asignatura) {

		LOGGER.info("AsigController, Method 'showSignUpForm'({})'", asignatura.getId());
		return "add-asignatura";
	}

	@PostMapping("/addasignatura")
	public String addUser(@Valid Asignatura asignatura, BindingResult result, Model model) {

		LOGGER.info("AsigController, Method 'addUser'({})'", asignatura.getId());
		if (result.hasErrors()) {
			return "add-asignatura";
		}

		asignaturaService.saveOrUpdate(asignatura);
		model.addAttribute("asignaturas", asignaturaService.getAllColegios());
		return "index-asignaturas";
	}

	@GetMapping("/editAsignatura/{id}")
	public String showUpdateForm(@PathVariable("id") long id, Model model) {

		LOGGER.info("AsigController, Method 'showUpdateForm'({})'", id);
		Asignatura asignatura = asignaturaService.findById(id);

		model.addAttribute("asignatura", asignatura);
		return "update-asignatura";
	}

	@PostMapping("/updateAsignatura/{id}")
	public String updateUser(@PathVariable("id") long id, @Valid Asignatura asignatura, BindingResult result,
			Model model) {

		LOGGER.info("AsigController, Method 'updateUser'({})'", id);
		if (result.hasErrors()) {
			asignatura.setId(id);
			return "update-asignatura";
		}

		asignaturaService.saveOrUpdate(asignatura);
		model.addAttribute("asignaturas", asignaturaService.getAllColegios());
		return "index-asignaturas";
	}

	@GetMapping("/deleteAsignatura/{id}")
	public String deleteUser(@PathVariable("id") Long id, Model model) {
		
		LOGGER.info("AsigController, Method 'deleteUser'({})'", id);
		Asignatura asignatura = asignaturaService.findById(id);
		asignaturaService.deleteById(id);
		model.addAttribute("asignaturas", asignaturaService.getAllColegios());
		return "index-asignaturas";
	}
}
