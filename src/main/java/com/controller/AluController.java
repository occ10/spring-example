package com.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.entity.Alumno;
import com.service.AlumnoService;

@Controller
public class AluController {

	private static final Logger LOGGER = LoggerFactory.getLogger(AluController.class);

	@Autowired
	private AlumnoService alumnoService;

	@GetMapping("/crearAlumno")
	public String showSignUpForm(Alumno alumno) {
		
		LOGGER.info("AluController, Method 'showSignUpForm'({})'", alumno.getId());
		return "add-alumno";
	}

	@PostMapping("/addalumno")
	public String addAlumno(@Valid Alumno alumno, BindingResult result, Model model) {
		
		LOGGER.info("AluController, Method 'addAlumno'({})'", alumno.getId());
		if (result.hasErrors()) {
			return "add-alumno";
		}

		alumnoService.saveOrUpdate(alumno);
		model.addAttribute("alumnos", alumnoService.getAllAlumnos());
		return "index-alumnos";
	}

	@GetMapping("/editAlumno/{id}")
	public String showUpdateForm(@PathVariable("id") long id, Model model) {
		
		LOGGER.info("AluController, Method 'showUpdateForm'({})'", id);
		Alumno alumno = alumnoService.findById(id);

		model.addAttribute("alumno", alumno);
		return "update-alumno";
	}

	@PostMapping("/updateAlumno/{id}")
	public String updateUser(@PathVariable("id") long id, @Valid Alumno alumno, BindingResult result, Model model) {
		
		LOGGER.info("AluController, Method 'updateUser'({})'", id);
		if (result.hasErrors()) {
			alumno.setId(id);
			return "update-user";
		}

		alumnoService.saveOrUpdate(alumno);
		model.addAttribute("alumnos", alumnoService.getAllAlumnos());
		return "index-alumnos";
	}

	@GetMapping("/deleteAlumno/{id}")
	public String deleteUser(@PathVariable("id") Long id, Model model) {
		
		LOGGER.info("AluController, Method 'deleteUser'({})'", id);
		Alumno alumno = alumnoService.findById(id);
		alumnoService.deleteById(id);
		model.addAttribute("alumnos", alumnoService.getAllAlumnos());
		
		return "index-alumnos";
	}
}
