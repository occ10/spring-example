package com.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.dto.CourseCreationDto;
import com.entity.Colegio;
import com.entity.CourseRegistration;
import com.pojo.ExamenAlu;
import com.repository.CourseRegistrationRipository;
import com.service.AlumnoService;
import com.service.AsignaturaService;
import com.service.impl.CourseRegistrationServiceImpl;

@Controller
public class ExamenController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ExamenController.class);

	@Autowired
	CourseRegistrationServiceImpl courseRegistrationServiceImpl;

	@GetMapping("/consultaAlumnos/{id}")
	public String consulta(@PathVariable("id") Long id, Model model) {

		LOGGER.info("ExamenController, Method 'consulta'({})'", id);
		List<CourseRegistration> registroExamen = this.courseRegistrationServiceImpl.findByAsig(id);
		CourseCreationDto courseForm = new CourseCreationDto();

		for (int i = 0; i < registroExamen.size(); i++) {
			courseForm.addExamenalu(new ExamenAlu(registroExamen.get(i)));
		}

		model.addAttribute("registroExamen", courseForm);

		return "examen";
	}

	@Transactional
	@PostMapping("/notaExamen")
	public String update(@ModelAttribute CourseCreationDto notasAlumnos, Model model) {

		LOGGER.info("ExamenController, Method 'update'({})'", "");
		this.courseRegistrationServiceImpl.setgradeOfStudent(notasAlumnos);

		return "index";

	}

}
