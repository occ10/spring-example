package com.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.pojo.AluAssist;
import com.service.AlumnoService;

@Controller
public class AluAssistController {

	private static final Logger LOGGER = LoggerFactory.getLogger(AluAssistController.class);
	
	@Autowired
	AlumnoService alumnoService;
	
	@RequestMapping("/faltasAlumno")
	public String consulta(Model model) {
		
		LOGGER.info("AluAssistController, Method 'consulta'({})'", "");
		model.addAttribute("alumnos", this.alumnoService.getAllAlumnos());
		model.addAttribute("aluAssist", new AluAssist());

		return "assistencia-alumno";
	}
	
	@RequestMapping(value = "/salvarFaltas", method = RequestMethod.POST)
	public String salvar(AluAssist aluAsist) {
		
		LOGGER.info("AluAssistController, Method 'salvar'({})'", aluAsist.getAlumnoId());
		this.alumnoService.asignAbscence(aluAsist);
		
		return "index-asignaturas";
	}
	
}
