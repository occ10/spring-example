package com.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.entity.Profesor;
import com.service.impl.ProfesorServiceImpl;

import exception.NotFoundException;

@RestController
@RequestMapping("/profesor")
public class ProfesorController {

	@Autowired
	private ProfesorServiceImpl profesorService;

	private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

	@GetMapping(value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public Profesor findById(@PathVariable(value = "id") Long id) {

		LOGGER.info("ProfesorController, Method 'findById'({})'", id);
		return profesorService.findById(id);
	}

	@PostMapping(produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseStatus(HttpStatus.CREATED)
	public Profesor save(@RequestBody final Profesor profesor) {

		LOGGER.info("ProfesorController, Method 'save'({})'", profesor.getId());
		return profesorService.saveOrUpdate(profesor);
	}

	@PostMapping(value = "/save", produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseStatus(HttpStatus.CREATED)
	public List<Profesor> saveAll(@RequestBody final List<Profesor> profesores) {
		
		LOGGER.info("ProfesorController, Method 'saveAll'({})'", "");
		return profesorService.saveOrUpdateAll(profesores);
	}

	@PutMapping(value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public Profesor update(@RequestBody final Profesor profesor, @PathVariable final Long id) {
		
		LOGGER.info("ProfesorController, Method 'update'({})'", id);
		return this.profesorService.saveOrUpdate(profesor);
	}

	@DeleteMapping(value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public void delete(@PathVariable final Long id) {
		
		LOGGER.info("ProfesorController, Method 'delete'({})'", id);

		try {
			this.profesorService.deleteById(id);
		} catch (IllegalArgumentException e) {
			throw new NotFoundException("profesor con id " + id + " no se ha podido borrar");
		}
	}
}
