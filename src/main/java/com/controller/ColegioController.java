package com.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.entity.Colegio;
import com.service.impl.ColegioServiceImpl;

import exception.NotFoundException;

@RestController
@RequestMapping("/colegio")
public class ColegioController {

	@Autowired
	private ColegioServiceImpl colegioService;

	private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

	@GetMapping(value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public Colegio findById(@PathVariable(value = "id") Long id) {
		
		LOGGER.info("ColegioController, Method 'findById'({})'", id);
		return colegioService.findById(id);
	}

	@PostMapping(produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseStatus(HttpStatus.CREATED)
	public Colegio save(@RequestBody final Colegio colegio) {
		
		LOGGER.info("ColegioController, Method 'save'({})'", colegio.getId());
		return colegioService.saveOrUpdate(colegio);
	}
	
	@PostMapping(value = "/save", produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseStatus(HttpStatus.CREATED)
	public List<Colegio> saveAll(@RequestBody final List<Colegio> colegios) {
		
		LOGGER.info("ColegioController, Method 'saveAll'({})'", "");
		return colegioService.saveOrUpdateAll(colegios);
	}

	@PutMapping(value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public Colegio update(@RequestBody final Colegio colegio, @PathVariable final Long id) {
		
		LOGGER.info("ColegioController, Method 'update'({})'", id);
		return this.colegioService.saveOrUpdate(colegio);
		
	}

	@DeleteMapping(value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public void delete(@PathVariable final Long id) {
		
		LOGGER.info("ColegioController, Method 'delete'({})'", id);
		LOGGER.info("Method 'delete'({})'", id);
		
		try{
			this.colegioService.deleteById(id);
		}catch(IllegalArgumentException e) {
			throw new NotFoundException("Colegio con id " + id + " no se ha podido borrar");
		}	
	}
}
