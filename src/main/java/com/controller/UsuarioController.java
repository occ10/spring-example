package com.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.entity.User;
import com.repository.UserRepository;
import com.service.UserService;

@Controller
public class UsuarioController {

	private static final Logger LOGGER = LoggerFactory.getLogger(UsuarioController.class);

	@Autowired
	private UserService userService;

	@Autowired
	UserRepository userRepository;

	@GetMapping("/signup")
	public String showSignUpForm(User user) {
		
		LOGGER.info("UsuarioController, Method 'showSignUpForm'({})'", user.getId());
		return "add-user";
	}

	@PostMapping("/adduser")
	public String addUser(@Valid User user, BindingResult result, Model model) {
		
		LOGGER.info("UsuarioController, Method 'addUser'({})'", user.getId());
		
		if (result.hasErrors()) {
			return "add-user";
		}

		userService.saveOrUpdate(user);
		model.addAttribute("users", userService.findAll());
		return "index";
	}

	@GetMapping("/edit/{id}")
	public String showUpdateForm(@PathVariable("id") long id, Model model) {
		
		LOGGER.info("UsuarioController, Method 'showUpdateForm'({})'", id);
		User user = userService.findById(id);

		model.addAttribute("user", user);
		return "update-user";
	}

	@PostMapping("/update/{id}")
	public String updateUser(@PathVariable("id") long id, @Valid User user, BindingResult result, Model model) {
		
		LOGGER.info("UsuarioController, Method 'updateUser'({})'", id);
		
		if (result.hasErrors()) {
			user.setId(id);
			return "update-user";
		}

		userService.saveOrUpdate(user);
		model.addAttribute("users", userService.findAll());
		
		return "index";
	}

	@GetMapping("/delete/{id}")
	public String deleteUser(@PathVariable("id") Long id, Model model) {
		
		LOGGER.info("UsuarioController, Method 'deleteUser'({})'", id);
		User user = userService.findById(id);
		userService.deleteUser(id);
		model.addAttribute("users", userService.findAll());
		
		return "index";
	}
}
