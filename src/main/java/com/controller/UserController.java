package com.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.entity.User;
import com.service.UserService;

import exception.NotFoundException;

@RestController
@RequestMapping("/api/user")
public class UserController {

	@Autowired
	private UserService userService;

	private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

	@GetMapping(value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public User findById(@PathVariable(value = "id") Long id) {
		
		LOGGER.info("UserController, Method 'findById'({})'", id);
		return userService.findById(id);
	}

	@PostMapping(produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseStatus(HttpStatus.CREATED)
	public User save(@RequestBody final User user) {
		
		LOGGER.info("UserController, Method 'save'({})'", user.getId());
		return userService.saveOrUpdate(user);
	}

	@PutMapping(value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public User update(@RequestBody final User user, @PathVariable final Long id) {
		
		LOGGER.info("UserController, Method 'update'({})'", user.getId());
		user.setId(id);
		return this.userService.saveOrUpdate(user);
	}

	@DeleteMapping(value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public void delete(@PathVariable final Long id) {
		
		LOGGER.info("UserController, Method 'delete'({})'", id);

		try {
			this.userService.deleteUserById(id);
		} catch (IllegalArgumentException e) {
			throw new NotFoundException("User con id " + id + " no se ha podido borrar");
		}
	}

	@PostMapping(value = "/addcolumn")
	public void addColumn() {
		
		LOGGER.info("UserController, Method 'addColumn'({})'", "");
		userService.addLastNameColumn();
	}

}
