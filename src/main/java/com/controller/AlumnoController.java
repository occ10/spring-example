package com.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.entity.Alumno;
import com.entity.DeleteResponse;
import com.service.impl.AlumnoServiceImpl;

import exception.NotFoundException;

@RestController
@RequestMapping("/alumno")
public class AlumnoController {

	@Autowired
	private AlumnoServiceImpl alumnoService;

	private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

	@GetMapping(value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public Alumno findById(@PathVariable(value = "id") Long id) {
		
		LOGGER.info("AlumnoController, Method 'findById'({})'", id);
		return alumnoService.findById(id);
	}

	@PostMapping(produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseStatus(HttpStatus.CREATED)
	public Alumno save(@RequestBody final Alumno alumno) {
		
		LOGGER.info("AlumnoController, Method 'save'({})'", alumno.getId());
		return alumnoService.saveOrUpdate(alumno);
	}

	@PostMapping(value = "/save", produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseStatus(HttpStatus.CREATED)
	public List<Alumno> saveAll(@RequestBody final List<Alumno> alumnos) {
		
		LOGGER.info("AlumnoController, Method 'saveAll'({})'", "");
		LOGGER.info("Method 'saveAll' called");
		return alumnoService.saveOrUpdateAll(alumnos);
	}

	@PutMapping(value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public Alumno update(@RequestBody final Alumno alumno, @PathVariable final Long id) {
		
		LOGGER.info("AlumnoController, Method 'update'({})'", id);

		return this.alumnoService.saveOrUpdate(alumno);
	}

	@DeleteMapping(value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public DeleteResponse delete(@PathVariable final Long id) {
		LOGGER.info("AlumnoController, Method 'delete'({})'", id);

		try {
			this.alumnoService.deleteById(id);
			return new DeleteResponse("objeto borrado correctamente");
		} catch (IllegalArgumentException e) {
			throw new NotFoundException("alumno con id " + id + " no se ha podido borrar");
		}
	}
}
