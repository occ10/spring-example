package com.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.entity.FaltaAssistencia;

@Repository
public interface AssistenciaRepository extends JpaRepository<FaltaAssistencia, Long> {
	
}
