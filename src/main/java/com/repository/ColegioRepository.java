package com.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.entity.Colegio;

@Repository
public interface ColegioRepository extends JpaRepository<Colegio, Long> {//que calase estamos utilizando y el id de esa clase
	
}