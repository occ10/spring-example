package com.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.entity.User;


@Repository
public interface UserRepository extends CrudRepository<User, Long> {//que calase estamos utilizando y el id de esa clase
	
	List<User> findDistinctByName(String name);
	
	@Query("select u from User u where u.email = ?1")
	User findByEmail(String email);

	@Query(value = "select * from User where email = ?1", nativeQuery = true)
	User findByEmailNative(String email);
	
	@Modifying 
	@Query("update User u set u.name = ?1 where u.id = ?2")
	int setFixedFirstNameFor(String firstName, Long id);
	
	@Modifying 
	@Query("delete from User u where u.id = ?1")
	void deleteUserById(Long id);
	
	@Modifying 
	@Query(value = "alter table user add column lastName varchar(20) not null", nativeQuery = true)
	void addLastNameColumn();
}
