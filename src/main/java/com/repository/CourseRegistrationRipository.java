package com.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.entity.CourseRegistration;
import com.entity.FaltaAssistencia;

@Repository
public interface CourseRegistrationRipository extends JpaRepository<CourseRegistration, Long> {// que calase estamos
																								// utilizando y el id de
																								// esa clase
	@Query(value = "select * from Course_Registration where id = ?1", nativeQuery = true)
	List<CourseRegistration> findByAsig(Long idAsig);

	@Modifying
	@Query("update CourseRegistration cr set cr.grade = ?1 where cr.id = ?2")
	int setgradeOfStudent(Double grade, Long id);
}
