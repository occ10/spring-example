package com.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.entity.Profesor;

@Repository
public interface ProfesorRepository extends JpaRepository<Profesor, Long> {//que calase estamos utilizando y el id de esa clase
	
}
